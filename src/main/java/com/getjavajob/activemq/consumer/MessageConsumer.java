package com.getjavajob.activemq.consumer;

import com.getjavajob.activemq.domain.Message;
import com.getjavajob.activemq.repository.MessageRepository;
import com.getjavajob.activemq.util.MailSender;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;

@Component
@EnableJms
@Slf4j
public class MessageConsumer {
    private final MessageRepository messageRepository;
    private final MailSender mailSender;

    @Value("${email.to}")
    private String emailTo;

    @Autowired
    public MessageConsumer(MessageRepository messageRepository, MailSender mailSender) {
        this.messageRepository = messageRepository;
        this.mailSender = mailSender;
    }

    @JmsListener(destination = "jms-queue")
    @SendTo("answer-jms-queue")
    public String listener(String text) {
        log.info("Listener: " + text);
        Message message = new Message();
        message.setText(text);
        messageRepository.save(message);
        mailSender.send(emailTo, "JMS", text);
        return "Email: " + emailTo + " Message: " + text;
    }
}
