package com.getjavajob.activemq.repository;

import com.getjavajob.activemq.domain.Message;
import org.springframework.data.repository.CrudRepository;

public interface MessageRepository extends CrudRepository<Message, Long> {
}
